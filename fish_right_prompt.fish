function fish_right_prompt
  # Customize the right prompt
	set --local exit_code $status  # save previous exit code
  set --local my_date (date +"%A %d %b, %H:%M:%S")
  echo
	if test $exit_code -ne 0
    # Command exited in error
		echo -e (style_error)$my_date
  else
    echo -e (style_default)$my_date
	end
  set_color normal
end
