# Customize fish prompt

function __user_host
  set -l content 
  if [ (id -u) = "0" ];
    echo -n (style_error)
  else
    echo -n (style_default)
  end
  echo -n $USER@(hostname|cut -d . -f 1) (set color normal)
end

function __exit_status
	set --local exit_code $status  # save previous exit code
  set -l exit_symbol "\xe2\xa6\xbf"
	if test $exit_code -ne 0
		echo -en (style_error)$exit_symbol "$exit_code" (set_color normal) 
  else
		echo -en (style_default)$exit_symbol (set_color normal) 
  end
end

function __current_working_dir
  set -l symbol "\xe2\x96\xa9\xe2\x96\xb6"
  echo -en (style_default)(pwd)(set_color normal) 
end

function _git_branch_name
  echo (command git symbolic-ref HEAD 2> /dev/null | sed -e 's|^refs/heads/||')
end

function _git_is_dirty
  echo (command git status -s --ignore-submodules=dirty 2> /dev/null)
end

function __git_status
  if [ (_git_branch_name) ]
    set -l git_branch_symbol "\xee\x82\xa0"
    set -l git_branch (_git_branch_name)

    if [ (_git_is_dirty) ]
      set git_info $git_branch" *"
    else
      set git_info $git_branch
    end

    echo -en (style_light)" on "(style_default) $git_branch_symbol $git_info (set_color normal) 
  end
end

function __k8s_env
	if not set -q $K8S_ENV
    set -l k8s_symbol "\xe2\x8e\x88"
    set -l k8s_anchor_symbol "\xe2\x9a\x93"
    echo -en (style_light)" via "(style_default)$k8s_anchor_symbol(k8s_env)(set_color normal)
  end
end

function fish_prompt
  # Exit code / status
  __exit_status
	# user@host
	__user_host
	echo -n (style_light)" in "
  # Working dir
  __current_working_dir
  # git
	__git_status
  # kubernetes
	__k8s_env
	echo
	echo -e -n (style_default)"❯"
  echo (set_color normal)" "  # reset colors and end prompt  
end
